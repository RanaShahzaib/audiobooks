package com.shahzaib.audiobooks.Adapter;

/**
 * Created by Rana Shahzaib on 3/4/2018.
 */

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.shahzaib.audiobooks.R;
import com.shahzaib.audiobooks.SongModel;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import dm.audiostreamer.MediaMetaData;

public class MusicAdapter extends BaseAdapter {
    private List<SongModel> musicList;
    private Context mContext;
    private LayoutInflater inflate;

    private DisplayImageOptions options;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    private ColorStateList colorPause;

    public MusicAdapter(Context context, List<SongModel> authors) {
        this.musicList = authors;
        this.mContext = context;
        this.inflate = LayoutInflater.from(context);
        this.colorPause = ColorStateList.valueOf(context.getResources().getColor(R.color.md_blue_grey_500_75));
    }

    public void refresh(List<SongModel> musicList) {
        if (this.musicList != null) {
            this.musicList.clear();
        }
        this.musicList.addAll(musicList);
        notifyDataSetChanged();
    }

    public void notifyPlayState(MediaMetaData metaData) {
        if (this.musicList != null && metaData != null) {
                for (int i = 0; i < this.musicList.size(); i++) {
                    if (this.musicList.get(i).getMediaId().equalsIgnoreCase(metaData.getMediaId())) {
                        musicList.get(i).setSongPlayState(PlaybackStateCompat.STATE_PLAYING);
                        Log.v("notifyPlayState", "Position : "+i+" State : PLAYING");
                    }else{
                        musicList.get(i).setSongPlayState(PlaybackStateCompat.STATE_PAUSED);
                        Log.v("notifyPlayState", "Position : "+i+" State : PAUSE");
                    }
                }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return musicList.size();
    }

    @Override
    public Object getItem(int i) {
        return musicList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        ViewHolder mViewHolder;
        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = inflate.inflate(R.layout.inflate_allsongsitem, null);
            mViewHolder.playState = (ImageView) convertView.findViewById(R.id.img_playState);
            mViewHolder.mediaTitle = (TextView) convertView.findViewById(R.id.text_mediaTitle);
            mViewHolder.MediaDesc = (TextView) convertView.findViewById(R.id.text_mediaDesc);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        SongModel media = musicList.get(position);

        if(media.getSongPlayState()==PlaybackStateCompat.STATE_PLAYING){
            Log.v("getView", "Position : "+position+" State : PLAY");
        }else{
            Log.v("getView", "Position : "+position+" State : PAUSE");
        }


        mViewHolder.mediaTitle.setText(media.getMediaTitle());
        mViewHolder.MediaDesc.setText(media.getMediaArtist());

        mViewHolder.playState.setImageDrawable(getDrawableByState(mContext, media.getSongPlayState()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listItemListener != null) {
                    if(media.getSongPlayState()!=PlaybackStateCompat.STATE_PLAYING){
                        listItemListener.onItemClickListener(musicList.get(position));
                        notifyPlayState(musicList.get(position));
                    }
                }
            }
        });

        return convertView;
    }

    public static class ViewHolder {

        public ImageView playState;
        public TextView mediaTitle;
        public TextView MediaDesc;
    }


    private Drawable getDrawableByState(Context context, int state) {
        switch (state) {
            case PlaybackStateCompat.STATE_NONE:
                Drawable pauseDrawable = ContextCompat.getDrawable(context, R.drawable.ic_play);
                return pauseDrawable;
            case PlaybackStateCompat.STATE_PLAYING:
                Drawable play = ContextCompat.getDrawable(context, R.drawable.ic_pause);
                return play;
            case PlaybackStateCompat.STATE_PAUSED:
                Drawable playDrawable = ContextCompat.getDrawable(context, R.drawable.ic_play);
                return playDrawable;
            default:
                Drawable noneDrawable = ContextCompat.getDrawable(context, R.drawable.ic_pause);
                return noneDrawable;
        }
    }



    public void setListItemListener(ListItemListener listItemListener) {
        this.listItemListener = listItemListener;
    }

    public ListItemListener listItemListener;

    public interface ListItemListener {
        void onItemClickListener(MediaMetaData media);
        void pausePlayer(MediaMetaData media);
    }
}