package com.shahzaib.audiobooks

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
data class Book (var thumbnail : String,
                 var name : String,
                 var author : String,
                 var link : String,
                 var meta : String
)