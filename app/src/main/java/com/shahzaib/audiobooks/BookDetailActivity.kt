package com.shahzaib.audiobooks

import android.Manifest
import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.shahzaib.audiobooks.Helper.Helper
import com.shahzaib.audiobooks.Helper.JsonHelper
import com.shahzaib.audiobooks.UI.LoadingDialog
import kotlinx.android.synthetic.main.activity_book_detail.*
import org.json.JSONObject
import android.app.DownloadManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.support.v4.app.ActivityCompat
import dm.audiostreamer.MediaMetaData


class BookDetailActivity : AppCompatActivity() {

    var mChaptersList = ArrayList<SongModel>()
    lateinit var downloadLink:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)
        play.hide();
        loadBook();
    }

    private fun loadBook() {
        var loading = LoadingDialog()
        loading.show(supportFragmentManager, "Loading")
        AndroidNetworking.get(Helper.SINGLE_BOOK_URL+"?link="+intent.getStringExtra(JsonHelper.BOOK_LINK))
                .setTag("BookDetails")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        loading.dismiss()
                        play.show();
                        var thumb = response.getString(JsonHelper.THUMBNAIL)
                        name.text = response.getString(JsonHelper.BOOK_NAME)
                        description.text = response.getString(JsonHelper.BOOK_DETAILS)
                        author.text = response.getString(JsonHelper.BOOK_AUTHOR)
                        downloadLink = response.getString(JsonHelper.BOOK_DWONLOAD_LINK)
                        size.text = response.getString(JsonHelper.BOOK_SIZE)
                        duration.text = response.getString(JsonHelper.BOOK_DURATION)
                        catalog_date.text = response.getString(JsonHelper.BOOK_CATALOG_DATE)
                        read_by.text = response.getString(JsonHelper.BOOK_READ_BY)
                        download.setOnClickListener {
                            AlertDialog.Builder(this@BookDetailActivity, android.R.style.Theme_Material_Dialog)
                                    .setMessage("This book will be downloaded to your downloads directory. Are you sure you want to download?")
                                    .setTitle("Download")
                                    .setPositiveButton("download", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {

                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                if(checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                                                    ActivityCompat.requestPermissions(this@BookDetailActivity, Array<String>(1){ Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                                                }else{
                                                    download(downloadLink)
                                                }
                                            } else {
                                                download(downloadLink)
                                            }
                                        }
                                    })
                                    .setNegativeButton("cancel", object : DialogInterface.OnClickListener {
                                        override fun onClick(p0: DialogInterface?, p1: Int) {
                                            //
                                        }
                                    }).show()
                        }

                        mChaptersList = JsonHelper.parsePlaylist(response.getJSONArray(JsonHelper.PLAYLIST), mediaArt = thumb, bookName = name.text.toString())

                        play.setOnClickListener {
                            var intent = Intent(this@BookDetailActivity, PlayerActivity::class.java)
                            intent.putExtra(JsonHelper.BOOK_NAME, name.text.toString())
                            intent.putExtra(JsonHelper.BOOK_AUTHOR, author.text.toString())
                            intent.putExtra(JsonHelper.VAL_RESULTS, mChaptersList)
                            intent.putExtra(JsonHelper.BOOK_READ_BY, read_by.text.toString())
                            intent.putExtra(JsonHelper.THUMBNAIL, thumb)
                            startActivity(intent)
                        }

                        GlideApp.with(this@BookDetailActivity).load(thumb).centerCrop().fitCenter().into(thumbnail)
                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Toast.makeText(this@BookDetailActivity, anError?.message, Toast.LENGTH_SHORT).show()
                    }
                })

    }

    fun download(url:String){
        val r = DownloadManager.Request(Uri.parse(url))
        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name.text.toString()+".zip")
        r.allowScanningByMediaScanner()
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        dm.enqueue(r)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            download(downloadLink)
        }
    }
}
