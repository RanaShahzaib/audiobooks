package com.shahzaib.audiobooks

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
data class Chapter(var link:String,
                   var chapter:String,
                   var author:String,
                   var source:String,
                   var reader:String,
                   var time:String)