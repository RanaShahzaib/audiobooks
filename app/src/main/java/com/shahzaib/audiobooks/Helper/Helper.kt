package com.shahzaib.audiobooks.Helper

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
class Helper {
    companion object {
        val BASE_URL = "http://ufmo.asia/HLS/"
        val DASHBOARD_URL = BASE_URL+"librivox_dashboard_scrapper.php"
        val SEARCH_URL = BASE_URL+"librivox_search_scrapper.php"
        val SINGLE_BOOK_URL = BASE_URL+"single_book_view.php"
    }
}