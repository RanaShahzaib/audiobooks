package com.shahzaib.audiobooks.Helper

import com.shahzaib.audiobooks.Book
import com.shahzaib.audiobooks.Chapter
import com.shahzaib.audiobooks.SongModel
import dm.audiostreamer.MediaMetaData
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
class JsonHelper{
    companion object {
        val THUMBNAIL = "thumbnail"
        val BOOK_NAME = "book_name"
        val BOOK_AUTHOR = "book_author"
        val BOOK_LINK = "book_link"
        val BOOK_META = "book_meta"
        val BOOK_DETAILS = "book_description"
        val BOOK_DWONLOAD_LINK = "book_download_link"
        val BOOK_SIZE = "book_size"
        val BOOK_DURATION = "book_duration"
        val BOOK_CATALOG_DATE = "book_catalog_date"
        val BOOK_READ_BY = "book_read_by"

        val VAL_RESULTS = "results"
        val PLAYLIST = "playlist"

        val CHAPTER_LINK = "link"
        val CHAPTER_NAME = "chapter"
        val CHAPTER_AUTHOR = "author"
        val CHAPTER_SOURCE = "source"
        val CHAPTER_READER = "reader"
        val CHAPTER_TIME = "time"

        fun parseDashboardJSON(response: JSONObject) : ArrayList<Book>{
            var mList = ArrayList<Book>();
            var mResults = response.getJSONArray(VAL_RESULTS)
            for(i in 0..mResults.length()-1){
                var book =mResults.getJSONObject(i)
                mList.add(Book(book.getString(THUMBNAIL),
                        book.getString(BOOK_NAME),
                        book.getString(BOOK_AUTHOR),
                        book.getString(BOOK_LINK),
                        book.getString(BOOK_META)))
            }
            return mList
        }

        fun parseSearchJSON(mResults: JSONArray) : ArrayList<Book>{
            var mList = ArrayList<Book>();
            for(i in 0..mResults.length()-1){
                var book =mResults.getJSONObject(i)
                mList.add(Book(book.getString(THUMBNAIL),
                        book.getString(BOOK_NAME),
                        book.getString(BOOK_AUTHOR),
                        book.getString(BOOK_LINK),
                        book.getString(BOOK_META)))
            }
            return mList
        }

        fun parsePlaylist(mResults: JSONArray, mediaArt : String, bookName :String):ArrayList<SongModel>{
            var mList = ArrayList<SongModel>();
            for(i in 0..mResults.length()-1){
                var chapter =mResults.getJSONObject(i)

                var infoData = SongModel();
                infoData.setMediaId(""+i)
                infoData.setMediaUrl(chapter.getString(CHAPTER_LINK))
                infoData.setMediaTitle(chapter.getString(CHAPTER_NAME))
                infoData.setMediaArtist(chapter.getString(CHAPTER_AUTHOR))
                infoData.setMediaAlbum(bookName)
                infoData.setMediaComposer(CHAPTER_READER);
                infoData.setMediaArt(mediaArt)
                mList.add(infoData)
            }
            return mList
        }

    }
}