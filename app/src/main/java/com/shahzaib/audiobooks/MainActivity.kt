package com.shahzaib.audiobooks

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.shahzaib.audiobooks.Helper.Helper
import com.shahzaib.audiobooks.Helper.JsonHelper
import com.shahzaib.audiobooks.UI.LoadingDialog
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    var mList = ArrayList<Book>()
    var mList1 = ArrayList<Book>()
    var isLoading = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        recycler.adapter = DashboardAdapter(mList)
        recycler.layoutManager = GridLayoutManager(this, 2)
        search_recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if(!TextUtils.isEmpty(query)){
                    search(query)
                }
                return true
            }
        })
        search_view.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() {
                hint.text = "Search results"
                search_recyclerView.visibility = View.VISIBLE
            }

            override fun onSearchViewClosed() {
                hint.text = "Latest books"
                mList1.clear()
                try{
                    search_recyclerView.adapter.notifyDataSetChanged()
                    search_recyclerView.visibility = View.GONE
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        })
        loadDashboard()
    }

    private fun loadDashboard() {
        var loading = LoadingDialog()
        loading.isCancelable = false
        loading.show(supportFragmentManager, "Loading")
        AndroidNetworking.get(Helper.DASHBOARD_URL)
                    .setTag("Dashboard")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            try{
                                loading.dismiss()
                            }catch (ex: Exception) {
                                ex.printStackTrace()
                            }finally {
                                    mList = JsonHelper.parseDashboardJSON(response)
                                    recycler.adapter = DashboardAdapter(mList)
                            }

                        }

                        override fun onError(anError: ANError?) {
                            loading.dismiss()
                            Toast.makeText(this@MainActivity, "Oops!! Network error occurred", Toast.LENGTH_SHORT).show()
                        }
                    })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.main, menu);
        val item = menu?.findItem(R.id.action_search)
        search_view.setMenuItem(item)
        return true
    }

    fun search(query:String){
        if(!isLoading){
            isLoading = true
            var loading = LoadingDialog()
            mList1.clear()
            loading.show(supportFragmentManager, "Loading")
            AndroidNetworking.post(Helper.SEARCH_URL)
                    .setTag("DashboardSearch")
                    .setPriority(Priority.HIGH)
                    .addBodyParameter("q",query)
                    .build()
                    .getAsJSONArray(object : JSONArrayRequestListener {
                        override fun onResponse(response: JSONArray) {
                            isLoading = false
                            loading.dismiss()
                            mList1 = JsonHelper.parseSearchJSON(response)
                            search_recyclerView.adapter = SearchAdapter(mList1)
                        }

                        override fun onError(anError: ANError?) {
                            isLoading = false
                            loading.dismiss()
                            Toast.makeText(this@MainActivity, "Oops!! Network error occurred", Toast.LENGTH_SHORT).show()
                        }
                    })
        }
    }

    override fun onBackPressed() {
        if (search_view.isSearchOpen()) {
            search_view.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

}
