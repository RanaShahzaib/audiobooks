package com.shahzaib.audiobooks

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyAppGlideModule : AppGlideModule()