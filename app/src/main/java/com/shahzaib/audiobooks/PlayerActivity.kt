package com.shahzaib.audiobooks

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.shahzaib.audiobooks.Helper.JsonHelper
import kotlinx.android.synthetic.main.activity_player.*
import com.shahzaib.audiobooks.UI.Slider
import com.shahzaib.audiobooks.UI.PlayPauseView
import com.shahzaib.audiobooks.Adapter.MusicAdapter

import android.app.PendingIntent
import android.widget.*
import dm.audiostreamer.CurrentSessionCallback
import dm.audiostreamer.MediaMetaData
import android.support.v4.media.session.PlaybackStateCompat
import android.text.format.DateUtils
import android.util.Log
import android.media.MediaMetadataRetriever
import android.net.Uri
import com.shahzaib.audiostreamer.AudioPlaybackListener
import com.shahzaib.audiostreamer.AudioStreamingManager


class PlayerActivity : AppCompatActivity(), CurrentSessionCallback, View.OnClickListener, Slider.OnValueChangedListener {

    private var musicList: ListView? = null
    private var adapterMusic: MusicAdapter? = null
    private var btn_play: PlayPauseView? = null

    private var streamingManager: AudioStreamingManager? = null
    private var currentSong: MediaMetaData? = null
    lateinit var adapter:MusicAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        configAudioStreamer()
        audioPg.setOnValueChangedListener(this)
        name.text = intent.getStringExtra(JsonHelper.BOOK_NAME)
        author.text = intent.getStringExtra(JsonHelper.BOOK_AUTHOR)
        GlideApp.with(this).load(intent.getStringExtra(JsonHelper.THUMBNAIL)).centerCrop().fitCenter().into(thumb)
        GlideApp.with(this).load(intent.getStringExtra(JsonHelper.THUMBNAIL)).centerCrop().fitCenter().into(background_thumb)
        read.setText(intent.getStringExtra(JsonHelper.BOOK_READ_BY))

        playerButton.setOnClickListener { view ->
            playPauseEvent(view)
        }
        playerButton.Pause()


        addSongs()

        music_loading_progress.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                return;
            }
        })

    }

    private fun addSongs() {
        var songsList = intent.getParcelableArrayListExtra<SongModel>(JsonHelper.VAL_RESULTS)
        adapter = MusicAdapter(this, songsList)
        currentSong = songsList.get(0)
        playSong(currentSong!!)
        chapter_name.setText(currentSong!!.mediaTitle)
        playerButton.Play()
        streamingManager?.setMediaList(songsList as List<MediaMetaData>?)
        streamingManager?.setPlayMultiple(true);
        adapter.setListItemListener(object : MusicAdapter.ListItemListener{
            override fun onItemClickListener(media: MediaMetaData?) {
                if(streamingManager?.currentAudio!=media){
                    playerButton.Play()
                    playSong(media!!)
                    chapter_name.setText(media.mediaTitle)
                }
            }

            override fun pausePlayer(media: MediaMetaData?) {
                streamingManager!!.onPause()
                playerButton.Pause()
            }
        })
        playlist.adapter = adapter
        adapter.notifyPlayState(currentSong)
    }


    override fun onStart() {
        super.onStart()
        try {
            streamingManager?.subscribesCallBack(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if(streamingManager?.isPlaying()!!){
            notifyAdapter(streamingManager?.currentAudio!!)
        }
    }

    override fun onResume() {
        super.onResume()
        if(streamingManager?.isPlaying()!!){
            notifyAdapter(streamingManager?.currentAudio!!)
        }
    }


    public override fun onStop() {
        try {
            streamingManager?.unSubscribeCallBack()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onStop()
    }

    override fun onDestroy() {
        try {
            streamingManager?.unSubscribeCallBack()
            streamingManager?.cleanupPlayer(true, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.onDestroy()
    }

    override fun onValueChanged(value: Int) {
        streamingManager?.onSeekTo(value.toLong());
        streamingManager?.scheduleSeekBarUpdate();
    }

    override fun onClick(p0: View?) {
    }

    override fun currentSeekBarPosition(p0: Int) {
        audioPg?.setValue(p0);
        setPGTime(p0.toLong());
    }

    override fun playCurrent(p0: Int, p1: MediaMetaData) {
        p1.mediaDuration = streamingManager?.duration?.toString();
        showMediaInfo(p1)
        notifyAdapter(p1);
    }


    override fun playNext(p0: Int, p1: MediaMetaData?) {
    }

    override fun playPrevious(p0: Int, p1: MediaMetaData?) {

    }

    override fun playSongComplete() {
        val timeString = "00.00"
        //time_total_bottom.setText(timeString)
        //time_total_slide.setText(timeString)
        //time_progress_bottom.setText(timeString)
        //time_progress_slide.setText(timeString)
        //audioPg.setValue(0)
    }

    override fun updatePlaybackState(p0: Int) {
        when(p0){
            PlaybackStateCompat.STATE_PLAYING-> {
                music_loading_progress.visibility = View.GONE
                playerButton.Play()
                chapter_name.setText(currentSong?.mediaTitle)
                if (currentSong != null) {
                    currentSong?.setPlayState(PlaybackStateCompat.STATE_PLAYING);
                    notifyAdapter(currentSong!!)
                }
            }
            PlaybackStateCompat.STATE_PAUSED->{
                music_loading_progress.visibility = View.GONE
                btn_play?.Pause();
                if (currentSong != null) {
                    currentSong?.setPlayState(PlaybackStateCompat.STATE_PAUSED)
                    notifyAdapter(currentSong!!);
                }
            }
            PlaybackStateCompat.STATE_NONE->{
                currentSong?.setPlayState(PlaybackStateCompat.STATE_NONE)
                notifyAdapter(currentSong!!);
            }
            PlaybackStateCompat.STATE_STOPPED->{
                btn_play?.Pause();
                audioPg?.setValue(0);
                if (currentSong != null) {
                    currentSong?.setPlayState(PlaybackStateCompat.STATE_NONE)
                    notifyAdapter(currentSong!!)
                }
            }
            PlaybackStateCompat.STATE_BUFFERING->{
                music_loading_progress.visibility = View.VISIBLE
                if (currentSong != null) {
                    currentSong?.setPlayState(PlaybackStateCompat.STATE_NONE)
                    notifyAdapter(currentSong!!);
                }
            }
        }
    }

    private fun playPauseEvent(v: View) {
        if (streamingManager!!.isPlaying()) {
            streamingManager!!.onPause()
            (v as PlayPauseView).Pause()
        } else {
            streamingManager?.onPlay(currentSong)
            (v as PlayPauseView).Play()
        }
    }

    private fun playSong(media: MediaMetaData) {
        streamingManager?.onPlay(media)
        media.mediaDuration = streamingManager?.duration.toString();
        showMediaInfo(media)
    }

    private fun showMediaInfo(media: MediaMetaData) {
        currentSong = media
        audioPg!!.setValue(0)
        audioPg!!.setMin(0)
        audioPg!!.max = 0;
        setPGTime(0)
    }

    private fun configAudioStreamer() {
        streamingManager = AudioStreamingManager.getInstance(this)
        streamingManager?.setShowPlayerNotification(false)
        streamingManager?.setPendingIntentAct(null)
        streamingManager?.isPlayMultiple = true
        streamingManager?.setShowPlayerNotification(true)
        streamingManager?.setPendingIntentAct(getNotificationPendingIntent())
    }


    private fun getNotificationPendingIntent(): PendingIntent {
        intent.putExtra("pending", true)
        return PendingIntent.getActivity(this@PlayerActivity, 0, intent, 0)
    }

    private fun notifyAdapter(media: MediaMetaData) {
        adapter.notifyPlayState(media)
    }

    fun setPGTime(progress:Long){
        try {
            if(audioPg.max==0){
                audioPg!!.setMax(AudioPlaybackListener.mMediaPlayer?.duration!!);
            }
            var timeString = "00:00"
            currentSong = streamingManager?.getCurrentAudio()
            if (currentSong != null && progress !== (currentSong!!.getMediaDuration().toLong())) {
                timeString = DateUtils.formatElapsedTime(progress / 1000)
                endTime.text = DateUtils.formatElapsedTime(AudioPlaybackListener.mMediaPlayer?.duration!!.toLong()/1000)
            }
            startTime.text = timeString
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        streamingManager?.cleanupPlayer(true, true)
    }
}