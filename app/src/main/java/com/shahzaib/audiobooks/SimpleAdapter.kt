package com.shahzaib.audiobooks

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.shahzaib.audiobooks.Helper.JsonHelper

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
public class DashboardAdapter: RecyclerView.Adapter<DashboardAdapter.VH> {
    var list = ArrayList<Book>();

    constructor(list: ArrayList<Book>){
        this.list = list
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardAdapter.VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.book_simple_card, parent, false))
    }

    override fun onBindViewHolder(holder: DashboardAdapter.VH, position: Int) {
        var book = list.get(position)
        holder.bookTitle.text = book.name
        holder.bookAuthor.text = book.author
        GlideApp.with(holder.itemView).load(book.thumbnail).centerCrop().fitCenter().into(holder.bookThubmail)

        holder.itemView.setOnClickListener {
            var intent = Intent(holder.itemView.context, BookDetailActivity::class.java)
            intent.putExtra(JsonHelper.BOOK_LINK, book.link)
            holder.itemView.context.startActivity(intent)
            //Toast.makeText(holder.itemView.context, book.link, Toast.LENGTH_SHORT).show()
        }
    }
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var bookThubmail:ImageView;
        var bookTitle:TextView;
        var bookAuthor:TextView;

        init {
            bookThubmail = itemView.findViewById<ImageView>(R.id.thumb)
            bookTitle = itemView.findViewById(R.id.title)
            bookAuthor = itemView.findViewById(R.id.author)
        }
    }
}

public class SearchAdapter: RecyclerView.Adapter<SearchAdapter.VH> {
    var list = ArrayList<Book>();

    constructor(list: ArrayList<Book>){
        this.list = list
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.search_book_card, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        var book = list.get(position)
        holder.bookTitle.text = book.name
        holder.bookAuthor.text = book.author
        holder.meta.text = book.meta
        GlideApp.with(holder.itemView).load(book.thumbnail).centerCrop().fitCenter().into(holder.bookThubmail)
        holder.itemView.setOnClickListener {
            var intent = Intent(holder.itemView.context, BookDetailActivity::class.java)
            intent.putExtra(JsonHelper.BOOK_LINK, book.link)
            holder.itemView.context.startActivity(intent)
        }
    }
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var bookThubmail:ImageView;
        var bookTitle:TextView;
        var bookAuthor:TextView;
        var meta:TextView;

        init {
            bookThubmail = itemView.findViewById<ImageView>(R.id.thumb)
            bookTitle = itemView.findViewById(R.id.title)
            bookAuthor = itemView.findViewById(R.id.author)
            meta = itemView.findViewById(R.id.meta)
        }
    }
}