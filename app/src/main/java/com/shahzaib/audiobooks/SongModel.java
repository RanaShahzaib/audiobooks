package com.shahzaib.audiobooks;

import android.os.Parcel;
import android.os.Parcelable;

import dm.audiostreamer.MediaMetaData;

/**
 * Created by ccn on 22/03/2018.
 */

public class SongModel extends MediaMetaData{
    int songPlayState;

    public int getSongPlayState() {
        return songPlayState;
    }

    public void setSongPlayState(int songPlayState) {
        this.songPlayState = songPlayState;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getMediaId());
        dest.writeString(getMediaUrl());
        dest.writeString(getMediaTitle());
        dest.writeString(getMediaArtist());
        dest.writeString(getMediaAlbum());
        dest.writeString(getMediaComposer());
        dest.writeString(getMediaDuration());
        dest.writeString(getMediaArt());
        dest.writeInt(getPlayState());
        dest.writeInt(songPlayState);
    }

    public SongModel(){

    }

    protected SongModel(Parcel in) {
        setMediaId(in.readString());
        setMediaUrl(in.readString());
        setMediaTitle(in.readString());
        setMediaArtist(in.readString());
        setMediaAlbum(in.readString());
        setMediaComposer(in.readString());
        setMediaDuration(in.readString());
        setMediaArt(in.readString());
        setPlayState(in.readInt());
        songPlayState = in.readInt();
    }

    public static final Creator<SongModel> CREATOR = new Creator<SongModel>() {
        public SongModel createFromParcel(Parcel in) {
            return new SongModel(in);
        }

        public SongModel[] newArray(int size) {
            return new SongModel[size];
        }
    };
}
