package com.shahzaib.audiobooks.UI

import android.content.Context
import android.support.v4.widget.TextViewCompat
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by Rana Shahzaib on 3/3/2018.
 */
class RanaTextView : TextView {
    constructor(context: Context?):super(context)
    constructor(context: Context?, attrs: AttributeSet?):super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int):super(context, attrs, defStyleAttr, defStyleRes)
}